<?php
error_reporting(0);

if ($_GET['api']) {
$api = $_GET['api'];
}else{
$api = "";
}

$defsize = "256";
$defsqm = "65536";

$db_address = "localhost";
$db_username = "username";
$db_password = "password";
$db_name = "robust";
$mysqli = new mysqli($db_address, $db_username, $db_password, $db_name);
if ($mysqli->connect_errno) {
    die("Connect failed: %s\n". $mysqli->connect_error);
}

$sizextotal = "";
$sizeytotal = "";
$sizetotal = "";
$totalsingleregions = "";
$sizeq = $mysqli->query("SELECT * FROM regions");
$totalregions = $sizeq->num_rows;
while($sizer = $sizeq->fetch_array(MYSQLI_BOTH)) {
	$x = $sizer['sizeX'];
	$y = $sizer['sizeY'];
	$sizextotal += $x;
	$sizeytotal += $y;
	$xytotal = $x * $y;
	$sizetotal += $xytotal;
	if ($xytotal >= $defsqm) {
		$totalsingleregions += $xytotal / $defsqm;
	}else if($xytotal == $defsqm) {
		++$totalsingleregions;
	}
}
$sizeq->close();

$region256countq = $mysqli->query("SELECT * FROM regions WHERE sizeX = '$defsize' AND sizeY = '$defsize'");
$region256count = $region256countq->num_rows;
$region256countq->close();

$regionvarcountq = $mysqli->query("SELECT * FROM regions WHERE sizeX > '$defsize' AND sizeY > '$defsize'");
$regionvarcount = $regionvarcountq->num_rows;
$regionvarcountq->close();

if ($api) {
	if ($api == "totalsingleregions") {
		echo $totalsingleregions;
	}
	if ($api == "totalregions") {
		echo $totalregions;
	}
	if ($api == "region256count") {
		echo $region256count;
	}
	if ($api == "regionvarcount") {
		echo $regionvarcount;
	}
	if ($api == "sizetotal") {
		echo $sizetotal;
	}
	$apiarray = array('TotalSingleRegions' => $totalsingleregions, 'TotalRegions' => $totalregions, 'Total256Regions' => $region256count, 'TotalVarRegions' => $regionvarcount, 'LandMass' => $sizetotal);
	if ($api == "json") {
		echo json_encode($apiarray);
	}
	if ($api == "xml") {
		echo xmlrpc_encode($apiarray);
	}
	if ($api == "lsl") {
		echo number_format($totalregions)."=".number_format($totalsingleregions)."=".number_format($region256count)."=".number_format($regionvarcount)."=".number_format($sizetotal);
	}
}else{
	$totalsingleregionswording = "<a href='#' id='tooltip' data-toggle='tooltip' title='Total region count, counting total land in terms of regular size regions.'>(".number_format($totalsingleregions).")</a>";
	echo "
	<p>
	<h2>Land Mass Count</h2>
	This grid has <B>".number_format($totalregions)."</B> regions ".$totalsingleregionswording.".<br>
	There are <B>".number_format($region256count)."</B> regular size regions.<br>
	There are <B>".number_format($regionvarcount)."</B> var regions.<br>
	Total land mass: <B>".number_format($sizetotal)."</B> square meters.<br>
	</p>
	";
}

$mysqli->close();
?>