Land Mass Counter is very easy to setup and use.
Just drop it in your website folder and put in your database connect info inside the file.
Then just visit the page in any browser to see your land mass and region count.
For developers the API is very simple.
Just send a GET request to the page with api= which ever format you want.
Examples:
landmass.php?api=json < this will display the stats in the json format.
landmass.php?api=xml < this will do the same thing but in xml_rpc format.

It will return the stats as...
TotalRegions -- Total regions on the grid.
Total256Regions -- Total regions that are 256 quare meters.
TotalVarRegions -- Total regions that are var regions.
LandMass - Total land mass in square meters of ALL regions/sims.

Any problems please feel free to email me at contact@zetamex.com